#!/usr/bin/env @lua@/bin/lua

function system_cmd(the_cmd)
  local output = ''
  do
    local h = io.popen(the_cmd)
    output = h:read('*a')
    h:close()
  end
  output = output:match('^\n*([^\n]+)\n*$')
  return output
end

function usage()
  print('Usage:')
  print('  tag [(+|-)TAG] FILES...')
  print('')
  print('  where TAG is the tag to add (+) or remove (-) and FILES... are the files to act upon')
  print('  if (+|-)TAG is not provided, tag will try to create the requested file with the tags the directory')
end

if not arg[1] or not arg[2] then usage() os.exit(1) end

function file_path_validate(path)
  local path_opened = io.open(path, 'r')
  if path_opened == nil then return nil end
  io.close(path_opened)
  return path
end

function get_root(root_file_identifier, root)
  while file_path_validate(root .. '/' .. root_file_identifier) == nil
  do
    root = root:match('(.*)[/][^/]+')
    if root == nil then return end
  end

  if file_path_validate(root .. '/' .. root_file_identifier) == nil then return nil end

  return root
end


local root = get_root('.tup', system_cmd('pwd'))
if not root then print('Not in a tup environment!') os.exit(1) end

-----------------------
-- handle rm command --
-----------------------

if arg[1] == "rm" or arg[1] == 'remove' then
  local removes = {}

  table.remove(arg, 1)
  arg[0], arg[-1] = nil, nil

  for k, file in pairs(arg)
  do
    realfile = system_cmd('realpath ' .. file)

    if not realfile
    then
      print('invalid path: ' .. file)
      os.exit(1)
    end

    if not file_path_validate(realfile)
    then
      print(realfile .. ' does not exist!')
      os.exit(1)
    end

    if realfile:sub(1, #root + #"/store/") ~= root .. '/store/'
    then
      print(realfile .. ' is not in the store!')
      os.exit(1)
    end

    table.insert(removes, 'rm ' .. realfile)
  end

  for k, remove in pairs(removes)
  do
    -- print('performing: ' .. remove)
    os.execute(remove)
  end

  -- os.execute('tup &>/dev/null') -- conflicts with any watcher that runs tup

  os.exit(0)
end
--------------------------
-- handle touch command --
--------------------------

if arg[1] == "touch" then
  local touches = {}

  table.remove(arg, 1)
  arg[0], arg[-1] = nil, nil

  for k, file in pairs(arg)
  do
    realfile = system_cmd('realpath ' .. file)

    if not realfile
    then
      print('invalid path: ' .. file)
      os.exit(1)
    end

    if file_path_validate(realfile)
    then
      print(realfile .. ' exists!')
      os.exit(1)
    end

    local dirname, filename = realfile:match('(.*)[/]([^/]+)$')

    -- print('checking for .workspaces in ' .. dirname)
    local workspaces_root = get_root('.workspaces', dirname)
    if not workspaces_root then print('Not in workspaces!') os.exit(1) end

    local tags = {}
    local main_tag = realfile:sub(1 + #workspaces_root):match('^[/]([^/]+)[/]')

    if main_tag
    then
      table.insert(tags, main_tag)
      -- print("main tag is " .. main_tag .. ' for ' .. realfile)
    end

    local sub_tags = realfile:sub(1 + #workspaces_root):match('^[/][^/]+[/]([^/]+)[/]')
    if sub_tags
    then
      for tag in sub_tags:gmatch('[@]([^@]+)')
      do
        table.insert(tags, tag)
        -- print("sub tag is " .. tag .. ' for ' .. realfile)
      end
    end

    table.sort(tags)

    tagstring = ''
    for _, tag in pairs(tags)
    do
      tagstring = tagstring .. tag .. '@'
    end

    local store_path = root .. '/store/' .. tagstring .. filename

    if file_path_validate(store_path)
    then
      print(store_path .. ' already exists!')
      os.exit(1)
    end

    table.insert(touches, 'touch ' .. root .. '/store/' .. tagstring .. filename)
  end
  for k, touch in pairs(touches)
  do
    -- print('performing: ' .. touch)
    os.execute(touch)
  end

  -- os.execute('tup &>/dev/null') -- conflicts with any watcher that runs tup

  os.exit(0)
end

--------------------------
-- handle +-TAG command --
--------------------------

action, tag = (arg[1] or ''):match("^([+-])([a-zA-Z]+)$")
if not action or not tag then usage() os.exit(1) end

table.remove(arg, 1)
arg[0], arg[-1] = nil, nil

 -- check that all files exists and are in the .tup store
for k, file in pairs(arg)
do
  local realfile = system_cmd('realpath ' .. file)

  if not file_path_validate(file) or not file_path_validate(realfile) or not realfile:match('^' .. root .. '/store/') then
    print(file .. ' (' .. realfile .. ') is not a valid tup store path')
    os.exit(1)
  end

  arg[k] = realfile
end

function tags_and_filename(s)
  -- print('handling ' .. s)
  local filename = s:match("([^@]+)$")
  local rest = s:match("(.+[@])[^@]+$")
  local a = {}
  if rest then
    for w in rest:gmatch("([^@]+)[@]") do table.insert(a, w) end
    table.sort(a)
  end
  return a, filename
end

function tags_remove(tags, tag)
  for k, t in pairs(tags)
  do
    if t == tag then table.remove(tags, k) return end
  end
end

function tags_add(tags, tag)
  for _, t in pairs(tags)
  do
    if t == tag then return end
  end
  table.insert(tags, tag)
  table.sort(tags)
end

local moves = {}

-- for each file: sort the tags and perform the action, then mv the file to its new name
for _, path in pairs(arg)
do
  local thedir, oldname = path:match("^(.*[/])([^/]+)$")
  local globaltag = oldname:match("^([@])")
  if globaltag then oldname = oldname:match("^[@](.+)$") end
  -- print('old name is ' .. oldname)
  local tags, filename = tags_and_filename(oldname)

  if (action == '+')
  then
    tags_add(tags, tag)
  elseif (action == '-')
  then
    tags_remove(tags, tag)
  else
    print('Error!!')
    os.exit(1)
  end

  local tagstring = globaltag or ''

  for k, v in pairs(tags)
  do
    tagstring = tagstring .. v .. '@'
  end

  if file_path_validate(thedir .. tagstring .. filename)
  then
    print("Applying " .. action .. tag .. " would overwrite existing file " .. thedir .. tagstring .. filename)
    os.exit(1)
  end

  table.insert(moves, 'mv ' .. path .. ' ' .. thedir .. tagstring .. filename)
end

for k, move in pairs(moves)
do
  -- print('performing: ' .. move)
  os.execute(move)
end

-- os.execute('tup &>/dev/null') -- conflicts with any watcher that runs tup

os.exit(0)

