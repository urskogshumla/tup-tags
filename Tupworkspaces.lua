function fill_workspaces(workspaces_dir, store_dir)

  tup.rule({}, 'touch %o', { workspaces_dir .. '.workspaces/active' })

  function tags_and_filename(s)
      local rest, filename = s:match("(.+[@])([^@]+)$")
      a = {}
      for w in rest:gmatch("([^@]+)[@]") do table.insert(a, w) end
      table.sort(a)
      return a, filename
  end

  global_key = {}
  tags = {}

  for _, sourcefile_path in pairs(tup.glob(store_dir .. '*'))
  do
    local sourcefile = sourcefile_path:match("[^/]+$")

    local notags = sourcefile:match("^[^@]+$")
    if notags ~= nil then
      print('link ' .. sourcefile .. ' to workspaces')
      tup.rule
      (
        { store_dir .. sourcefile },
        'ln -s ../%f %o',
        { workspaces_dir .. sourcefile }
      )
    else
      for tag in sourcefile:gmatch("([^@]*)[@]") do
        if tag == '' then tag = global_key end
        if tags[tag] == nil then tags[tag] = {} end
        table.insert(tags[tag], sourcefile)
      end
    end
  end

  for tag, files in pairs(tags)
  do
    if tag != global_key then
      print(tag .. ' has ' ..tostring(files))
      local workspace_dep_file = workspaces_dir .. '.workspaces/' .. tag .. '.workspace'

      for k, file in pairs(files)
      do
        files[k] = store_dir .. file
        -- print('a file is ' .. files[k])
      end

      tup.rule(files, 'touch %o', { workspace_dep_file })

      for k, file in pairs(files)
      do
        local sourcefiletags, filename = tags_and_filename(file:match("[^/]+$"))
        table.sort(sourcefiletags)
        tagstring = ''
        subs = '../../'
        for k, v in pairs(sourcefiletags) do
          if v ~= tag then
            tagstring = tagstring .. '@' .. v
            subs = '../../../'
          end
        end
        print('subs is ' .. subs .. ' and file is ' .. file .. ' and workspaces_dir is ' .. workspaces_dir)
        print('making a rule for ' .. subs .. file .. ' -> ' .. workspaces_dir .. tag .. '/' .. tagstring .. '/' .. filename)
        tup.rule
        (
          { file, extra_inputs = { workspace_dep_file } },
          'ln -s ' .. subs .. '%f %o',
          { workspaces_dir .. tag .. '/' .. tagstring .. '/' .. filename }
        )
        print('done with ' .. subs .. file)
      end
    end
  end
end
