# Overlapping folders for a flat tup compilation hierarchy

>   This needs a `store` directory to even run!

This is a *prototype* for working with a non hierarchical file structure, similar
to how tags and categories are used in blogs today. To get started, run tup as
a background watch job, like so:

```bash
$ while true ; do tup &>/dev/null ; sleep 0.3 ; done &
```

...and to be really fancy; also add a live view in another terminal of the 
full folder hierarchy:

```bash
$ LC_CTYPE=C watch -ctn 0.3 tree -C
```

We also need access to the `tag` tool for indirectly changing the files in the
`store`. Make sure you have `lua` installed for running it.

```bash
$ PATH="$(realpath helpers):$PATH"
```

And then we're off! Try navigating into the newly generated workspaces folder.
Here one needs to be a bit careful, but not too badly. The basics are as follows:

```bash
$ cd workspaces

$ tag touch my_first_file.md

$ ls
my_first_file.md

$ vi my_first_file.md    # edit normally as it is a symbolic link

$ cat my_first_file.md
Hello World!

$ tag touch kitten.md stray_cat.md house_cat.md dog.md puppy.md

$ ls
dog.md  house_cat.md  kitten.md  my_first_file.md  puppy.md  stray_cat.md

$ tag +animal *.md

$ ls
animal

$ cd animal/

$ ls
dog.md  house_cat.md  kitten.md  my_first_file.md  puppy.md  stray_cat.md

$ tag -animal my_first_file.md

$ ls
dog.md  house_cat.md  kitten.md  puppy.md  stray_cat.md

$ tag +cat *cat.md kitten.md

$ tag +dog dog.md puppy.md

$ ls
@cat  @dog

$ ls ..
animal  cat  dog  my_first_file.md

$ ls @cat/
house_cat.md  kitten.md  stray_cat.md

$ ls @dog/
dog.md  puppy.md

$ tag +cute @cat/kitten.md @dog/puppy.md

$ cd ..

$ ls
animal  cat  cute  dog  my_first_file.md

$ cd cute

$ ls
@animal@cat  @animal@dog

$ tree
.
├── @animal@cat
│   └── kitten.md -> ../../../store/animal@cat@cute@kitten.md
└── @animal@dog
    └── puppy.md -> ../../../store/animal@cute@dog@puppy.md

2 directories, 2 files

$ cd ..

$ ls
animal  cat  cute  dog  my_first_file.md

$ tag rm my_first_file.md

$ ls
animal  cat  cute  dog

$ tree
.
├── animal
│   ├── @cat
│   │   ├── house_cat.md -> ../../../store/animal@cat@house_cat.md
│   │   └── stray_cat.md -> ../../../store/animal@cat@stray_cat.md
│   ├── @cat@cute
│   │   └── kitten.md -> ../../../store/animal@cat@cute@kitten.md
│   ├── @cute@dog
│   │   └── puppy.md -> ../../../store/animal@cute@dog@puppy.md
│   └── @dog
│       └── dog.md -> ../../../store/animal@dog@dog.md
├── cat
│   ├── @animal
│   │   ├── house_cat.md -> ../../../store/animal@cat@house_cat.md
│   │   └── stray_cat.md -> ../../../store/animal@cat@stray_cat.md
│   └── @animal@cute
│       └── kitten.md -> ../../../store/animal@cat@cute@kitten.md
├── cute
│   ├── @animal@cat
│   │   └── kitten.md -> ../../../store/animal@cat@cute@kitten.md
│   └── @animal@dog
│       └── puppy.md -> ../../../store/animal@cute@dog@puppy.md
└── dog
    ├── @animal
        │   └── dog.md -> ../../../store/animal@dog@dog.md
            └── @animal@cute
                    └── puppy.md -> ../../../store/animal@cute@dog@puppy.md

14 directories, 12 files

$ 
```

## Future

Maybe a FUSE file system, so that the user can use ordinary cp/mv/touch commands
via modified read/write/create syscalls? That would be cute, and also not rely on
a constantly running build tool.

